unit uCliente;

interface

uses
 dcEntidade, DCORM.Mapping.Attributes, DCORM.Mapping.RegisterClass, uEndereco, Generics.Collections, uDependente, uCartoesCredito;

 type
   [Entidade]
   TCliente = class(TEntidade)
  private
  {$REGION 'Campos'}
    Fidade: integer;
    [Campo('nomecliente',100)]
    Fnome: string;
    Ftelefone: string;
    [ChaveEstrangeira('id_endereco')]
    Fendereco: TEndereco;
    [ListaEstrangeira('cliente_dependete_id')]
    Fdependentes: TObjectList<TDependente>;
    Fcnpj: string;
    [ChaveEstrangeira('id_cartaocredito')]
    Fcartoscredito: TCartosCredito;
  {$ENDREGION}
  public
     property nome: string read Fnome write Fnome;
     property idade: integer read Fidade write Fidade;
     property telefone: string read Ftelefone write Ftelefone;
     property endereco: TEndereco read Fendereco write Fendereco;
     property dependentes: TObjectList<TDependente> read Fdependentes write Fdependentes;
     property cnpj:string  read Fcnpj write Fcnpj;
     property cartoscredito: TCartosCredito read Fcartoscredito write Fcartoscredito;
 end;


implementation

{ TCliente }


{ TCliente }


{ TCliente }


initialization
  TEntityRegister.register(TCliente);

end.
