unit uCartoesCredito;

interface

uses
 dcEntidade, DCORM.Mapping.Attributes, DCORM.Mapping.RegisterClass;

 type
   [Entidade]
    TCartosCredito = class(TEntidade)
  private
  {$REGION 'Campos'}
    Fnumero: string;
    [Campo('numero',150)]
    Flimite: Double;
    [Campo('Limite',18,6)]
  {$ENDREGION}

  published
      property numero: string read Fnumero write Fnumero;
      property Limite: Double read FLimite write FLimite;
  end;

implementation

{ TCartosCredito }


end.
