unit uDependente;

interface

 uses
   dcEntidade, DCORM.Mapping.Attributes, DCORM.Mapping.RegisterClass;

  type
   [Entidade]
   TDependente = class(TEntidade)
  private
    Fidade: integer;
    Fnome: string;
  public
     property nome: string read Fnome write Fnome;
     property idade: integer read Fidade write Fidade;
   end;

implementation

{ TDependente }

initialization
  TEntityRegister.register(TDependente);

end.
