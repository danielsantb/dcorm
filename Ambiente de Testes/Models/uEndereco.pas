unit uEndereco;

interface

 uses
   dcEntidade, DCORM.Mapping.Attributes, DCORM.Mapping.RegisterClass;

  type
    [Entidade]
    TEndereco = class(TEntidade)
  private
    Fbairro: string;
    Fnumero: string;
    Frua: string;
    Fcep: string;
  public
      property rua: string read Frua write Frua;
      property numero: string read Fnumero write Fnumero;
      property bairro: string read Fbairro write Fbairro;
      property cep: string read Fcep write Fcep;
    end;

implementation

{ TEndereco }


initialization
  TEntityRegister.register(TEndereco);


end.
