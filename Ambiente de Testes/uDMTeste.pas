unit uDMTeste;

interface

uses
  System.SysUtils, System.Classes, uADStanIntf, uADStanOption, uADStanError,
  uADGUIxIntf, uADPhysIntf, uADStanDef, uADStanPool, uADStanAsync,
  uADPhysManager, uADGUIxFormsWait, uADPhysODBCBase, uADPhysMSSQL, uADCompGUIx,
  uADPhysPG, Data.DB, uADCompClient, DBAccess, Uni, UniProvider,
  PostgreSQLUniProvider, Data.SqlExpr, SQLServerUniProvider, Data.DBXMSSQL;

type
  TDMTeste = class(TDataModule)
    SqlServer: TADConnection;
    ADPhysPgDriverLink1: TADPhysPgDriverLink;
    ADGUIxWaitCursor1: TADGUIxWaitCursor;
    ADPhysMSSQLDriverLink1: TADPhysMSSQLDriverLink;
    SQLConnection1: TSQLConnection;
    PostgreSQLUniProvider1: TPostgreSQLUniProvider;
    UniConnection1: TUniConnection;
    SQLServerUniProvider1: TSQLServerUniProvider;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMTeste: TDMTeste;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
