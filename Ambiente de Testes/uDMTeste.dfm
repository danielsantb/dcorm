object DMTeste: TDMTeste
  OldCreateOrder = False
  Height = 405
  Width = 611
  object SqlServer: TADConnection
    Params.Strings = (
      'Database=DB_DCORM'
      'User_Name=sa'
      'Password=*101sp03*'
      'Server=Server'
      'DriverID=MSSQL')
    LoginPrompt = False
    Left = 51
    Top = 32
  end
  object ADPhysPgDriverLink1: TADPhysPgDriverLink
    VendorHome = 'I:\Database'
    Left = 43
    Top = 240
  end
  object ADGUIxWaitCursor1: TADGUIxWaitCursor
    Left = 51
    Top = 144
  end
  object ADPhysMSSQLDriverLink1: TADPhysMSSQLDriverLink
    Left = 48
    Top = 192
  end
  object SQLConnection1: TSQLConnection
    DriverName = 'MSSQL'
    LoginPrompt = False
    Params.Strings = (
      'SchemaOverride=%.dbo'
      'DriverUnit=Data.DBXMSSQL'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DBXCommonDriver180.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=18.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXMsSqlMetaDataCommandFactory,DbxMSSQLDr' +
        'iver180.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXMsSqlMetaDataCommandFact' +
        'ory,Borland.Data.DbxMSSQLDriver,Version=18.0.0.0,Culture=neutral' +
        ',PublicKeyToken=91d62ebb5b0d1b1b'
      'GetDriverFunc=getSQLDriverMSSQL'
      'LibraryName=dbxmss.dll'
      'VendorLib=sqlncli10.dll'
      'VendorLibWin64=sqlncli10.dll'
      'HostName=Diretoria01'
      'Database=DCORM'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'OSAuthentication=False'
      'PrepareSQL=True'
      'User_Name=DCORM'
      'BlobSize=-1'
      'ErrorResourceFile='
      'OS Authentication=False'
      'Prepare SQL=False'
      'Password=DCORM')
    Left = 475
    Top = 53
  end
  object PostgreSQLUniProvider1: TPostgreSQLUniProvider
    Left = 224
    Top = 165
  end
  object UniConnection1: TUniConnection
    ProviderName = 'PostgreSQL'
    Database = 'DB_DCORM'
    Username = 'postgres'
    Password = 'postgres'
    Server = 'localhost'
    LoginPrompt = False
    Left = 216
    Top = 37
  end
  object SQLServerUniProvider1: TSQLServerUniProvider
    Left = 224
    Top = 216
  end
end
