program AmbienteDeTestes;

uses
  Vcl.Forms,
  uPrincipal in 'uPrincipal.pas' {Form4},
  uCliente in 'Models\uCliente.pas',
  uEndereco in 'Models\uEndereco.pas',
  uDependente in 'Models\uDependente.pas',
  DCORM.Connectors.FireDAC in '..\ORM\Conectores\DCORM.Connectors.FireDAC.pas',
  DCORM.Connectors.UniDAC in '..\ORM\Conectores\DCORM.Connectors.UniDAC.pas',
  uCartoesCredito in 'Models\uCartoesCredito.pas',
  uDMTeste in 'uDMTeste.pas' {DMTeste: TDataModule},
  DCORM.Connectors.DBX in '..\ORM\Conectores\DCORM.Connectors.DBX.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TDMTeste, DMTeste);
  Application.Run;
end.
