unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  Vcl.StdCtrls, Data.SqlExpr, Bde.DBTables
  {,uADDAptManager, UniProvider, PostgreSQLUniProvider, DBAccess, Uni,
    uADGUIxFormsWait, uADCompGUIx, uADCompClient, uADStanIntf, uADStanOption,
    uADStanError, uADGUIxIntf, uADPhysIntf, uADStanDef, uADStanPool,
    uADStanAsync, uADPhysManager, uADPhysPG};

type
  TForm4 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure LogMemo(Log: String);
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

uses
  DCORM.Engine.DatabaseManager, DCORM.Interfaces.SQLS,
  DCORM.Engines.MSSQL,
  DCORM.Interfaces.Connectors,
  DCORM.Connectors.DBX,
  uDMTeste;

procedure TForm4.Button1Click(Sender: TObject);
var
  DBManager: TDatabaseManager;
  Engine:    ISQL;
  Conector:  IConnector;
begin
try
  Engine          := TMSSQLEngine.Create;
  Conector        := TDBXConnector.Create(DMTeste.SQLConnection1);
  DBManager       := TDatabaseManager.Create(Conector, Engine);
  DBManager.OnLog := LogMemo;
  DBManager.UpdateDatabase;
finally
  DBManager.Free;
end;
end;

procedure TForm4.Button2Click(Sender: TObject);
begin
// UniConnection1.GetFieldNames('Cliente',Memo1.Lines);
// ADConnection1.GetFieldNames('','','Cliente','',Memo1.Lines);
end;

procedure TForm4.LogMemo(Log: String);
begin
Memo1.Lines.Add(Log);
end;

end.
