unit DCORM.Connectors.FireDAC;

interface

  uses
   DCORM.Interfaces.Connectors, uADCompClient, Classes;

  type
    TFireDACConnector = class(TInterfacedObject,IConnector)
  private
    Fconnection: TADConnection;
    Lt: TStringList;
    LF: TStringList;
    procedure  GetTableNames;
    procedure  GetFieldNames(Tabela:string);
  public
    constructor Create(Connection: TADConnection);

    procedure ExecuteSQL(SQL: string);
    function isTableExist(Tabela: string): Boolean;
    function isFieldExist(Tabela: string; Campo: string): Boolean;
  end;

implementation

{ TFireDACConnector }
 uses
  System.SysUtils;


constructor TFireDACConnector.Create(Connection: TADConnection);
begin
  Fconnection := Connection;
end;

procedure TFireDACConnector.ExecuteSQL(SQL: string);
var
 Query: TADQuery;
begin
  try
    Query := TADQuery.Create(nil);
    Query.Connection := Fconnection;

    Query.SQL.Text := SQL;
    Query.ExecSQL;

    Query.Free;
  except
    on E: Exception do
      begin
        e.Message := 'Erro Conector FireDAC '+e.Message;
        raise;
      end;
  end;
end;

procedure TFireDACConnector.GetFieldNames(Tabela: string);
begin
  Lf := TStringList.Create;
  Fconnection.GetFieldNames('','',Tabela,'',lf);
end;

procedure TFireDACConnector.GetTableNames;
begin
  Lt := TStringList.Create;
  Fconnection.GetTableNames('','','',Lt);
end;

function TFireDACConnector.isFieldExist(Tabela, Campo: string): Boolean;
begin
  GetFieldNames(Tabela);
  Result := LF.IndexOf(Campo) > -1;
end;

function TFireDACConnector.isTableExist(Tabela: string): Boolean;
begin
 GetTableNames;
 Result := Lt.IndexOf(Tabela) > -1;
 Lt.Free;
end;

end.
