unit DCORM.Connectors.UniDAC;

interface


 uses
   Uni, DCORM.Interfaces.Connectors, Classes;

 type
   TUnidacConnector = class(TInterfacedObject,IConnector)
  private
    Fconnection: TUniConnection;
    Lt, Lf: TStringList;
    procedure  GetTableNames;
    procedure GetFieldNames(Tabela: string);
  public
    constructor Create(Connection: TUniConnection);
    procedure ExecuteSQL(SQL: string);
    function isTableExist(Tabela: string): Boolean;
    function isFieldExist(Tabela: string; Campo: string): Boolean;
  end;

implementation

uses
  System.SysUtils;

{ TUnidacConnector }

constructor TUnidacConnector.Create(Connection: TUniConnection);
begin
  Fconnection := Connection;
end;

procedure TUnidacConnector.ExecuteSQL(SQL: string);
var
 Query: TuniQuery;
begin
  try
    Query := TuniQuery.Create(nil);
    Query.Connection := Fconnection;

    Query.SQL.Text := SQL;
    Query.ExecSQL;

    Query.Free;
  except
    on E: Exception do
      begin
        e.Message := 'Erro Conector UniDAC '+e.Message;
        raise;
      end;
  end;
end;

procedure TUnidacConnector.GetFieldNames(Tabela: string);
begin
  Lf := TStringList.Create;
  Fconnection.GetFieldNames(Tabela,Lf);
end;

procedure TUnidacConnector.GetTableNames;
begin
  Lt := TStringList.Create;
  Fconnection.GetTableNames(Lt);
end;

function TUnidacConnector.isFieldExist(Tabela, Campo: string): Boolean;
begin
  GetFieldNames(Tabela);
  Result := Lf.IndexOf(Campo) > -1;
  Lf.Free;
end;

function TUnidacConnector.isTableExist(Tabela: string): Boolean;
begin
  GetTableNames;
  Result := Lt.IndexOf(Tabela) > -1;
  Lt.Free;
end;

end.
