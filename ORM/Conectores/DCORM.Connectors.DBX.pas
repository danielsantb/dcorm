unit DCORM.Connectors.DBX;

interface


 uses
   DCORM.Interfaces.Connectors, Classes, SQLExpr;

 type
   TDBXConnector = class(TInterfacedObject,IConnector)
  private
    Fconnection: TSQLConnection;
    Lt, Lf: TStringList;
    procedure  GetTableNames;
    procedure GetFieldNames(Tabela: string);
  public
    constructor Create(Connection: TSQLConnection);
    procedure ExecuteSQL(SQL: string);
    function isTableExist(Tabela: string): Boolean;
    function isFieldExist(Tabela: string; Campo: string): Boolean;
  end;

implementation

uses
  System.SysUtils;

{ TDBXConnector }

constructor TDBXConnector.Create(Connection: TSQLConnection);
begin
  Fconnection := Connection;
end;

procedure TDBXConnector.ExecuteSQL(SQL: string);
var
 Query: TSQLQuery;
begin
  try
    Query               := TSQLQuery.Create(nil);
    Query.SQLConnection := Fconnection;
    Query.SQL.Text      := SQL;
    Query.ExecSQL;

    Query.Free;
  except
    on E: Exception do
      begin
        e.Message := 'Erro Conector DBX '+e.Message;
        raise;
      end;
  end;
end;

procedure TDBXConnector.GetFieldNames(Tabela: string);
begin
  Lf := TStringList.Create;
  Fconnection.GetFieldNames(Tabela,Lf);
end;

procedure TDBXConnector.GetTableNames;
begin
  Lt := TStringList.Create;
  Fconnection.GetTableNames(Lt);
end;

function TDBXConnector.isFieldExist(Tabela, Campo: string): Boolean;
begin
  GetFieldNames(Tabela);
  Result := Lf.IndexOf(Campo) > -1;
  Lf.Free;
end;

function TDBXConnector.isTableExist(Tabela: string): Boolean;
begin
  GetTableNames;
  Result := Lt.IndexOf(Tabela) > -1;
  Lt.Free;
end;

end.
