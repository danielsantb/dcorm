unit DCORM.Utils.RTTI;

interface

uses
 Classes, RTTI;

type
  TRTTIUTils = class
    class function ExtractClass(Field: TRttiField): TClass;

  end;


implementation

uses
  System.StrUtils;
{ TRTTIUTils }

class function TRTTIUTils.ExtractClass(Field: TRttiField): TClass;
var
 Nome: String;
begin
  Nome := Field.FieldType.AsInstance.MetaclassType.ClassName;
  Nome := ReplaceStr(Nome,'TObjectList<','');
  Nome := ReplaceStr(Nome,'>','');

  Result := TRttiContext.Create.FindType(Nome).AsInstance.MetaclassType;
end;

end.
