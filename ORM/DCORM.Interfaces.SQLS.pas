unit DCORM.Interfaces.SQLS;

interface

  type
   ISQL = interface
     ['{91A4CAFD-449A-4E12-BCE8-557EAFA101A4}'] //<-- Cltr + Shit + G
     function CreateTable(Nome: string): String;
     function CreateTableExtra: string;
     function GetID: String;
     function GetDateTimeField(Nome: string):String;
     function GetCharVariantField(Nome: string; Tamanho: Integer): String;
     function GetIntegerField(Nome: string): String;
     function GetFloatField(Nome: string; Tamanho,Precisao:Integer): String;
     function AdicionaCampo(Tabela,Campo: string):String;
     function AdicionaChaveID(Tabela: string): String;
     function AdicionaChave(Tabela, NomeDaChave: String): String;
     function AdicionaChaveEstrangeira(Tabela, TabelaPai, NomeDaChave: String): String;
   end;

implementation

end.
