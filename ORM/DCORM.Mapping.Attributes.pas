unit DCORM.Mapping.Attributes;

interface


  type
    Entidade = class(TCustomAttribute);

    Tabela   = class(TCustomAttribute)
    private
      FNome: String;
    public
      constructor Create(Nome: string);

      property nome: string read FNome;
    end;

   Campo = class(TCustomAttribute)
  private
    FNome: string;
    FTamanho: Integer;
    FPrecisao: Integer;
    FDefault: string;
  public
      property Nome: string read FNome write FNome;
      property Padrao: string read FDefault write FDefault;
      property Tamanho: Integer read FTamanho write FTamanho;
      property Precisao: Integer read FPrecisao write FPrecisao;

      constructor Create(Nome: string); overload;
      constructor Create(Nome, Padrao: string); overload;
      constructor Create(Nome: string; Tamanho: Integer); overload;
      constructor Create(Nome: string; Tamanho,Precisao: Integer); overload;
  end;

   ChaveEstrangeira = class(TCustomAttribute)
  private
    FNome: String;
  public
    property Nome: String read FNome write FNome;

    constructor Create(Nome: string);
  end;

  ListaEstrangeira = class(TCustomAttribute)
  private
    FNome: string;
  public
    property Nome: string read FNome write FNome;

    constructor Create(Nome: string);
  end;


implementation

{ Tabela }

constructor Tabela.Create(Nome: string);
begin
  FNome := Nome;
end;


{ Campo }

constructor Campo.Create(Nome: string);
begin
  FNome := Nome;
end;
constructor Campo.Create(Nome, Padrao: string);
begin
  FNome    := Nome;
  FDefault := Padrao;
end;


constructor Campo.Create(Nome: string; Tamanho: Integer);
begin
  FNome := Nome;
  FTamanho := Tamanho;
end;

{ ChaveEstrangeira }


constructor Campo.Create(Nome: string; Tamanho, Precisao: Integer);
begin
  FNome := Nome;
  FTamanho := Tamanho;
  FPrecisao := Precisao;
end;

{ ChaveEstrangeira }

constructor ChaveEstrangeira.Create(Nome: string);
begin
 FNome := Nome;
end;

{ ListaEstrangeira }


{ ListaEstrangeira }

constructor ListaEstrangeira.Create(Nome: string);
begin
  FNome := Nome;
end;

end.
