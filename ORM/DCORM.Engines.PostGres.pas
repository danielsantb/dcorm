unit DCORM.Engines.PostGres;

interface

  uses
    Classes, DCORM.Interfaces.SQLS;

  type
    TPostGresEngine = class(TInterfacedObject,ISQL)
  public
    function CreateTable(Nome: string): string;

    function GetID: string;
    function GetDateTimeField(Nome: string): string;
    function GetCharVariantField(Nome: string; Tamanho: Integer): String;
    function GetFloatField(Nome: string; Tamanho,Precisao:Integer): String;
    function GetIntegerField(Nome: string): string;
    function AdicionaChaveID(Tabela: string): String;
    function AdicionaChave(Tabela: string; NomeDaChave: string): string;
    function AdicionaChaveEstrangeira(Tabela: string; TabelaPai: string; NomeDaChave: string): string;

    function CreateTableExtra: string;
    function AdicionaCampo(Tabela,Campo: string): string;
  end;

implementation

uses
  System.SysUtils;

{ TPostGresEngine }

function TPostGresEngine.AdicionaCampo(Tabela,Campo: String): string;
begin
  Result := 'ALTER TABLE '+Tabela+ ' add '+Campo;
end;

function TPostGresEngine.AdicionaChave(Tabela, NomeDaChave: string): string;
begin
  Result := 'ALTER TABLE '+Tabela+' add '+NomeDaChave+' integer';
end;

function TPostGresEngine.AdicionaChaveEstrangeira(Tabela, TabelaPai, NomeDaChave: string): string;
begin
 Result := 'ALTER TABLE '+Tabela+' add CONSTRAINT fk'+Tabela+'_'+TabelaPai+'_id FOREIGN KEY ('+NomeDaChave+') REFERENCES '+TabelaPai+' (id) MATCH SIMPLE'
end;

function TPostGresEngine.AdicionaChaveID(Tabela: string): String;
begin
  Result := 'ALTER TABLE '+Tabela+' add CONSTRAINT PK_'+Tabela+' PRIMARY KEY (id)'
end;

function TPostGresEngine.CreateTable(Nome: string): string;
begin
   Result := 'CREATE TABLE '+Nome;
end;

function TPostGresEngine.CreateTableExtra: string;
begin
  Result := ' WITH (OIDS=FALSE);';
end;

function TPostGresEngine.GetCharVariantField(Nome: string; Tamanho: Integer): String;
begin
  Result := Nome+' character varying('+InttoStr(Tamanho)+')';
end;

function TPostGresEngine.GetDateTimeField(Nome: string): string;
begin
  Result := Nome + ' time without time zone ';
end;

function TPostGresEngine.GetFloatField(Nome: string; Tamanho,Precisao: Integer): string;
begin
  Result := Nome + ' double precision';
end;

function TPostGresEngine.GetID: string;
begin
  Result := ' ID integer NOT NULL '
end;

function TPostGresEngine.GetIntegerField(Nome: string): string;
begin
  Result := Nome + ' integer ';
end;

end.
