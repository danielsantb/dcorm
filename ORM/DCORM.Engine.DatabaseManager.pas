unit DCORM.Engine.DatabaseManager;

interface

  uses
   RTTI, Classes, DCORM.Interfaces.SQLS, DCORM.Interfaces.Connectors;

  type
    DatabaseManagerListener = procedure(Log: string) of object;

    TDatabaseManager = class
    private
      FOnLog: DatabaseManagerListener;
      SQLEngine: ISQL;
      Connector: IConnector;
      procedure ConstroiClasse(Classe: TClass);
      procedure AtualizaClasse(Classe: TClass);
      function ListaCampos(Tipo: TRttiType): string;
      function GetFieldType(NomeCampo: TRttiField): string;
      function MontaChaveEstrangeiras(Classe: TClass; var SqlEstrangeira, SqlListaEstrangeira: TstringList; isUpdate: Boolean): String;
      procedure MontaListaEstrangeira(Classe: TClass; NomeChave, TabelaPai: string; var SqlListaEstrangeira: TstringList);
      procedure SetOnLog(const Value: DatabaseManagerListener);
      procedure Log(Texto: string);
    public
      constructor Create(Connection: IConnector; EngineSQL: ISQL);

      procedure UpdateDatabase;

      property OnLog: DatabaseManagerListener read FOnLog write SetOnLog;
    end;

implementation

uses
  DCORM.Mapping.Attributes, System.SysUtils, System.TypInfo, System.StrUtils, DCORM.Utils.RTTI;

constructor TDatabaseManager.Create(Connection: IConnector; EngineSQL: ISQL);
begin
  SQLEngine   := EngineSQL;
  Connector   := Connection;
end;

procedure TDatabaseManager.AtualizaClasse(Classe: TClass);
var
 RT: TRttiType;
 AT: TCustomAttribute;
 FD: TRttiField;
 strTabela,  Nome: string;

 ListaSQL, SQLEstrangeira, SQLListaEstrangeira: TStringList;
  I: Integer;

begin
  Log('Atualizando a Classe '+Classe.ClassName);

  strTabela := '';

  ListaSQL := TStringList.Create;
  SQLEstrangeira := TStringList.Create;
  SQLListaEstrangeira := TStringList.Create;

  RT := TRttiContext.Create.GetType(Classe);

  for AT in RT.GetAttributes do
  begin
     if AT is Tabela then
        strTabela := Tabela(AT).nome;
  end;

  if strTabela = EmptyStr then
     strTabela := Copy(RT.AsInstance.MetaclassType.ClassName,2,100);


  for FD in RT.GetFields do
  begin
      Nome := UpperCase(Copy(FD.Name,2,100));

      for AT in Fd.GetAttributes do
      begin
        if AT is Campo then
           Nome := Campo(At).Nome;
      end;

      if not FD.FieldType.IsInstance then
         Begin
         if not Connector.isFieldExist(strTabela,Nome) then
          begin
            ListaSQL.Add(SQLEngine.AdicionaCampo(strTabela,GetFieldType(FD)));
          end;
         End;
  end;

  MontaChaveEstrangeiras(Classe,SQLEstrangeira,SQLListaEstrangeira, True);

  Log(ListaSQL.Text);
  Log(SQLEstrangeira.Text);
  Log(SQLListaEstrangeira.Text);

  for I := 0 to ListaSQL.Count - 1 do
    begin
      Connector.ExecuteSQL(ListaSQL.Strings[I]);
    end;

  for I := 0 to SQLEstrangeira.Count - 1 do
    begin
      Log(SQLEstrangeira.Strings[I]);
      Connector.ExecuteSQL(SQLEstrangeira.Strings[I]);
    end;

  for I := 0 to SQLListaEstrangeira.Count - 1 do
    begin
      Log(SQLEstrangeira.Strings[I]);
      Connector.ExecuteSQL(SQLListaEstrangeira.Strings[I]);
    end;

end;

procedure TDatabaseManager.ConstroiClasse(Classe: TClass);
var
 SQL, strTabela, strCampos: string;
 RT: TRttiType;
 AT: TCustomAttribute;
 SQLEstrangeira, SQLListaEstrangeira: TStringList;
 I: Integer;
begin
  SQLEstrangeira      := TStringList.Create;
  SQLListaEstrangeira := TStringList.Create;

  strTabela := '';

  RT := TRttiContext.Create.GetType(Classe);

  for AT in RT.GetAttributes do
  begin
     if AT is Tabela then
        strTabela := Tabela(AT).nome;
  end;

  if strTabela = EmptyStr then
     strTabela := Copy(RT.AsInstance.MetaclassType.ClassName,2,100);

  if Connector.isTableExist(strTabela) then
     begin
      AtualizaClasse(Classe);
      Exit;
     end;

  SQL   := SQLEngine.CreateTable(strTabela);

  strCampos := ListaCampos(RT);

  SQL := SQL + '(' + strCampos+ ') '+ SQLEngine.CreateTableExtra;

  Log(SQL);

  Connector.ExecuteSQL(SQL);

  MontaChaveEstrangeiras(Classe,SQLEstrangeira,SQLListaEstrangeira,False);

  for I := 0 to SQLEstrangeira.Count - 1 do
    begin
      Log(SQLEstrangeira.Strings[I]);
      Connector.ExecuteSQL(SQLEstrangeira.Strings[I]);
    end;

  for I := 0 to SQLListaEstrangeira.Count - 1 do
    begin
      Log(SQLEstrangeira.Strings[I]);
      Connector.ExecuteSQL(SQLListaEstrangeira.Strings[I]);
    end;
end;

function TDatabaseManager.MontaChaveEstrangeiras(Classe: TClass; var SqlEstrangeira, SqlListaEstrangeira: TstringList; isUpdate: Boolean): String;
var
  RT: TRttiType;
  strTabela: String;
  AT: TCustomAttribute;
  FD: TRttiField;
begin
  RT := TRttiContext.Create.GetType(Classe);

  for AT in RT.GetAttributes do
  begin
     if AT is Tabela then
        strTabela := Tabela(AT).nome;
  end;

  if strTabela = EmptyStr then
     strTabela := Copy(RT.AsInstance.MetaclassType.ClassName,2,100);

  if not isUpdate then
     SqlEstrangeira.Add(SQLEngine.AdicionaChaveID(strTabela));

  for FD in RT.GetFields do
  begin
     for AT in FD.GetAttributes do
     begin
        if AT is ChaveEstrangeira then
           Begin
            if not Connector.isFieldExist(strTabela,ChaveEstrangeira(At).Nome) then
              Begin
                SqlEstrangeira.Add(SQLEngine.AdicionaChave(strTabela,ChaveEstrangeira(At).Nome));
                SqlEstrangeira.Add(SQLEngine.AdicionaChaveEstrangeira(strTabela,Copy(FD.Name,2,100),ChaveEstrangeira(At).Nome));
              End;
           End;
        if AT is ListaEstrangeira then
           Begin
              MontaListaEstrangeira(TRTTIUTils.ExtractClass(FD),ListaEstrangeira(AT).Nome,strTabela,SqlListaEstrangeira);
           End;
     end;
  end;

  Log(SqlListaEstrangeira.Text);

end;

procedure TDatabaseManager.MontaListaEstrangeira(Classe: TClass; NomeChave,  TabelaPai: String;  var SqlListaEstrangeira: TstringList);
var
 RT: TRttiType;
 AT: TCustomAttribute;
 strTabela: String;
begin
  RT := TRttiContext.Create.GetType(Classe);

  for AT in RT.GetAttributes do
  begin
     if AT is Tabela then
        strTabela := Tabela(AT).nome;
  end;

  if strTabela = EmptyStr then
     strTabela := Copy(RT.AsInstance.MetaclassType.ClassName,2,100);

  if not Connector.isFieldExist(strTabela,NomeChave) then
     Begin
        SqlListaEstrangeira.Add(SQLEngine.AdicionaChave(strTabela,NomeChave));
        SqlListaEstrangeira.Add(SQLEngine.AdicionaChaveEstrangeira(strTabela,TabelaPai,NomeChave));
     End;
end;

function TDatabaseManager.GetFieldType(NomeCampo: TRttiField): string;
var
 Nome: String;
 AT: TCustomAttribute;
begin

  Nome := UpperCase(Copy(NomeCampo.Name,2,100));

  if Nome = 'ID' then
     Exit(SQLEngine.GetID);

  if NomeCampo.FieldType.Name = 'TDateTime' then
     Exit(SQLEngine.GetDateTimeField(Nome));
  if NomeCampo.FieldType.TypeKind in [tkString,tkWString,tkLString, tkUString] then
     begin
       for AT in NomeCampo.GetAttributes do
       begin
         if AT is Campo then
            begin
              if Campo(AT).Tamanho > 0 then
                 Exit(SQLEngine.GetCharVariantField(Campo(AT).Nome,Campo(AT).Tamanho))
              else
                 Exit(SQLEngine.GetCharVariantField(Campo(AT).Nome,255))
            end;
       end;
        Exit(SQLEngine.GetCharVariantField(Nome,255))
     end;
  if NomeCampo.FieldType.TypeKind in [tkInteger, tkInt64] then
     Exit(SQLEngine.GetIntegerField(Nome));
  if NomeCampo.FieldType.TypeKind in [tkFloat] then
     begin
     for AT in NomeCampo.GetAttributes do
       begin
         if AT is Campo then
            begin
              if (Campo(AT).Tamanho > 0) and (Campo(AT).Precisao > 0) then
                 Exit(SQLEngine.GetFloatField(Nome,Campo(AT).Tamanho,Campo(AT).Precisao))
              else
                 Exit(SQLEngine.GetFloatField(Nome,18,6))
            end;
       end;
         Exit(SQLEngine.GetFloatField(Nome,18,6))
     end;

end;

function TDatabaseManager.ListaCampos(Tipo: TRttiType): string;
var
 RF: TRttiField;
 Lista: TStringList;
begin
  Lista := TStringList.Create;

  try
    Lista.Delimiter := ',';
    Lista.StrictDelimiter := True;

    for RF in Tipo.GetFields do
    begin
      if not (RF.FieldType.TypeKind = tkClass) then
         Lista.Add(GetFieldType(RF));
    end;

    Result := StringReplace(Lista.DelimitedText,'"','',[rfReplaceAll]);
  finally
    Lista.Free;
  end;
end;

procedure TDatabaseManager.Log(Texto: string);
begin
  if Assigned(FOnLog) then
     OnLog(Texto);
end;

procedure TDatabaseManager.SetOnLog(const Value: DatabaseManagerListener);
begin
  FOnLog := Value;
end;

procedure TDatabaseManager.UpdateDatabase;
var
 RPack: TRttiPackage;
 RT: TRttiType;
 AT: TCustomAttribute;
begin

  Log('Iniciando Busca das Classes...');

  for RPack in TRttiContext.Create.GetPackages do
  begin

    Log('Pacote Encontrado: '+RPack.Name);

    for RT in RPack.GetTypes do
    begin

      for AT in RT.GetAttributes do
      begin
        if AT is Entidade then
           Begin
             Log('Entidade Encontrada: '+RT.AsInstance.MetaclassType.ClassName);
             ConstroiClasse(RT.AsInstance.MetaclassType);
           End;
      end;
    end;
  end;
end;

end.
