unit DCORM.Engines.MSSQL;

interface

  uses
    Classes, DCORM.Interfaces.SQLS;

  type
    TMSSQLEngine = class(TInterfacedObject,ISQL)
  public
    function CreateTable(Nome: string): string;

    function GetID: string;
    function GetDateTimeField(Nome: string): string;
    function GetCharVariantField(Nome: string; Tamanho: Integer): String;
    function GetFloatField(Nome: string; Tamanho,Precisao:Integer): String;
    function GetIntegerField(Nome: string): string;
    function AdicionaChaveID(Tabela: string): String;
    function AdicionaChave(Tabela: string; NomeDaChave: string): string;
    function AdicionaChaveEstrangeira(Tabela: string; TabelaPai: string; NomeDaChave: string): string;

    function CreateTableExtra: string;
    function AdicionaCampo(Tabela,Campo: string): string;
  end;

implementation

uses
  System.SysUtils;

{ TPostGresEngine }

function TMSSQLEngine.AdicionaCampo(Tabela,Campo: String): string;
begin
  Result := 'ALTER TABLE '+Tabela+ ' add '+Campo;
end;

function TMSSQLEngine.AdicionaChave(Tabela, NomeDaChave: string): string;
begin
  Result := 'ALTER TABLE '+Tabela+' add '+NomeDaChave+' integer';
end;

function TMSSQLEngine.AdicionaChaveEstrangeira(Tabela, TabelaPai, NomeDaChave: string): string;
begin
 Result := 'ALTER TABLE '+Tabela+' add CONSTRAINT fk'+Tabela+'_'+TabelaPai+'_id FOREIGN KEY ('+NomeDaChave+') REFERENCES '+TabelaPai+' (id) '
end;

function TMSSQLEngine.AdicionaChaveID(Tabela: string): String;
begin
  Result := 'ALTER TABLE '+Tabela+' add CONSTRAINT PK_'+Tabela+' PRIMARY KEY (id)'
end;

function TMSSQLEngine.CreateTable(Nome: string): string;
begin
   Result := 'CREATE TABLE '+Nome;
end;

function TMSSQLEngine.CreateTableExtra: string;
begin
  Result := ' ';
end;

function TMSSQLEngine.GetCharVariantField(Nome: string; Tamanho: Integer): String;
begin
  Result := Nome+' varchar('+InttoStr(Tamanho)+')';
end;

function TMSSQLEngine.GetDateTimeField(Nome: string): string;
begin
  Result := Nome + ' datetime ';
end;

function TMSSQLEngine.GetFloatField(Nome: string; Tamanho,Precisao: Integer): string;
begin
  Result := Nome + ' decimal ('+InttoStr(Tamanho)+','+InttoStr(Precisao)+')';
end;

function TMSSQLEngine.GetID: string;
begin
  Result := ' ID integer NOT NULL '
end;

function TMSSQLEngine.GetIntegerField(Nome: string): string;
begin
  Result := Nome + ' integer ';
end;

end.
