unit DCORM.Engine.EntityManager;

interface
//  Create Read Updade Delete = CRUD

  uses
   DCORM.Interfaces.SQLS, DCORM.Interfaces.Connectors, RTTI;

  type

    EntityManagerListener = procedure(Log: string) of object;

    TEntityManager = class
    private
      SQLEngine: ISQL;
      Connector: IConnector;
      FOnLog: EntityManagerListener;
      procedure internalsave(Objeto: TObject);
      procedure Log(Msg: string);

      function GetFields(Instancia: TObject; RT: TRttitype): string;
    public
      procedure save(Objeto: TObject);
      procedure update(Objeto: TObject);
      procedure delete(objeto: TObject);
      function findbyID<T: class>(ID:Integer):T;

      constructor Create(Connection: IConnector; EngineSQL: ISQL);


      property OnLog: EntityManagerListener read FonLog write FOnLog;
    end;

implementation

uses
 DCORM.Mapping.Attributes, System.SysUtils, Classes, System.TypInfo, System.DateUtils;

constructor TEntityManager.Create(Connection: IConnector; EngineSQL: ISQL);
begin
  SQLEngine := EngineSQL;
  Connector := Connection;
end;

procedure TEntityManager.delete(objeto: TObject);
begin
 //

end;

function TEntityManager.findbyID<T>(ID: Integer): T;
begin
 //
end;

function TEntityManager.GetFields(Instancia: TObject; RT: TRttitype): string;
var
 FD: TRttiField;
 AT: TCustomAttribute;
 ListaCampos, ListaValores: TstringList;
begin

  ListaCampos := TStringList.Create;
  ListaCampos.Delimiter := ',';
  ListaCampos.StrictDelimiter := True;

  ListaValores := TStringList.Create;
  ListaValores.Delimiter := ',';
  ListaValores.StrictDelimiter := True;


  for FD in RT.GetFields do
  begin
     for AT in FD.GetAttributes do
     begin
       if AT is Campo then
          Begin
            ListaCampos.Add(Campo(AT).Nome);
            if FD.FieldType.TypeKind = tkInteger then
               ListaValores.Add(QuotedStr(IntToStr(FD.GetValue(Instancia).AsInteger)))
            else if FD.FieldType.Name = 'TDateTime' then
               ListaValores.Add(QuotedStr(SQLEngine.GetDateTimeType(FD.GetValue(Instancia).AsVariant)))
            else
               ListaValores.Add(QuotedStr(FD.GetValue(Instancia).AsString));
          End;
     end;
  end;

  Result := '('+ListaCampos.DelimitedText+') values ('+ListaValores.DelimitedText+')';

end;

procedure TEntityManager.internalsave(Objeto: TObject);
var
 SQL: string;
 strTabela: String;
 RT: TRttiType;
 AT: TCustomAttribute;
begin

  RT := TRttiContext.Create.GetType(Objeto.ClassInfo);


  for AT in RT.GetAttributes do
  begin
    if AT is Tabela then
      strTabela := Tabela(AT).nome;
  end;

  if strTabela = '' then
     raise Exception.Create('Classe '+Objeto.ClassName+' n�o configurada!');

  SQL := SQLEngine.GetInsertSQL(strTabela) + GetFields(Objeto,RT);

  Connector.ExecuteSQL(SQL);

  Log(SQL);

end;

procedure TEntityManager.Log(Msg: string);
begin
  if Assigned(FOnLog) then
     FOnLog(Msg);
end;

procedure TEntityManager.save(Objeto: TObject);
begin
 internalsave(Objeto);
end;

procedure TEntityManager.update(Objeto: TObject);
begin
 //
end;

end.
