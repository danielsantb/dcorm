unit DCORM.Interfaces.Connectors;

interface

  uses
    Classes;

  type
    IConnector = interface
      ['{0B365994-B340-41A6-B0B5-513FA95B40BF}']
      function isTableExist(Tabela: string):Boolean;
      function isFieldExist(Tabela, Campo: string):Boolean;
      procedure ExecuteSQL(SQL: string);
    end;

implementation

end.
